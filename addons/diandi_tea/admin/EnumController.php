<?php

namespace addons\diandi_tea\admin;

use addons\diandi_tea\models\enums\TeaWxappTypeEnum;
use api\controllers\AController;
use common\helpers\ResultHelper;

class EnumController extends AController
{

    function actionIndex(): array
    {
        $list = [
            'wxappType'=>TeaWxappTypeEnum::listOptions()
        ];
        return ResultHelper::json(200, '获取成功',$list);
    }
}