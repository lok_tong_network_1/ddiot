<?php

namespace addons\diandi_tea\models\enums;

use common\components\BaseEnum;

class TeaWxappTypeEnum extends BaseEnum
{
    const          operation = 1;

    const          tea = 2;
    const          chess = 3;
    const          study = 4;
    const          billiards = 5;
    const          conference = 6;


    /**
     * @var string message category
     *             You can set your own message category for translate the values in the $list property
     *             Values in the $list property will be automatically translated in the function `listData()`
     */
    public static $messageCategory = 'App';

    /**
     * @var array
     */
    public static $list = [
        self::operation => '综合运营',
        self::tea => '茶室',
        self::chess => '棋牌',
        self::study => '自习室',
        self::billiards => '台球',
        self::conference => '会议',
    ];

}