<?php

/**
 * @Author: Wang Chunsheng 2192138785@qq.com
 * @Date:   2020-03-02 21:40:25
 * @Last Modified by:   Wang chunsheng  email:2192138785@qq.com
 * @Last Modified time: 2022-10-28 20:27:56
 */

namespace admin\controllers;

use admin\models\forms\LoginForm;
use admin\models\forms\PasswordResetRequestForm as FormsPasswordResetRequestForm;
use admin\models\forms\ResendVerificationEmailForm;
use admin\models\forms\ResetPasswordForm;
use admin\models\forms\SignupForm;
use admin\models\forms\VerifyEmailForm;
use common\helpers\ErrorsHelper;
use common\helpers\MapHelper;
use common\helpers\ResultHelper;
use common\models\DdUser;
use common\models\User;
use diandi\admin\acmodels\AuthItem;
use diandi\admin\acmodels\AuthRoute;
use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;

/**
 * Site controllers.
 */
class SiteController extends AController
{
    public $modelClass = '';

    protected array $authOptional = ['login', 'logout', 'error', 'signup', 'request-password-reset', 'setpassword', 'relations'];
    public int $searchLevel = 0;

    /**
     * Displays homepage.
     *
     * @return array
     */
    public function actionIndex(): array
    {
        return ResultHelper::json(200, '获取成功');
    }


    public function actionLogout(): array
    {
        DdUser::updateAll([
            'is_login' => 0,
        ], ['id' => Yii::$app->user->identity->id]);

        Yii::$app->user->logout();

        return ResultHelper::json(200, '退出成功', [
            'url' => Url::to(['site/login']),
        ]);
    }

    public function actionSignup(): array
   {
        $model = new SignupForm();
        $data = [
            'username' =>\Yii::$app->request->input('username'),
            'email' =>\Yii::$app->request->input('email'),
            'password' =>\Yii::$app->request->input('password'),
        ];
        // p($model->load(Yii::$App->request->post()),$model->signup());
        if ($model->load($data, '') && $model->signup()) {
            return ResultHelper::json(200, '感谢您的注册，请验证您的邮箱');
        } else {
            $msg = ErrorsHelper::getModelError($model);

            return ResultHelper::json(400, '注册失败', (array)$msg);
        }
    }




    public function actionXiufu(): void
   {
        if (Yii::$app->request->input('type') == 1) {
            $AuthRoute = new AuthRoute();
            $list = AuthRoute::find()->alias('a')->leftJoin(AuthItem::tableName().' as c',
                'a.route_name=c.name'
            )->select(['a.id', 'c.id as item_id'])->asArray()->all();

            foreach ($list as $key => $value) {
                $_AuthRoute = clone $AuthRoute;
                $_AuthRoute->updateAll([
                    'item_id' => $value['item_id'],
                ], [
                    'id' => $value['id'],
                ]);
            }
        } elseif (Yii::$app->request->input('type') == 2) {
            $authItem = new AuthItem();

            $AuthRoute = AuthRoute::find()->asArray()->all();

            foreach ($AuthRoute as $key => $value) {
                $_authItem = clone $authItem;
                $_authItem->setAttributes([
                    'name' => $value['route_name'],
                    'is_sys' => $value['is_sys'],
                    'permission_type' => 0,
                    'description' => $value['description'],
                    'parent_id' => 0,
                    'permission_level' => $value['route_type'],
                    'data' => $value['data'],
                    'module_name' => $value['module_name'],
                ]);
                $_authItem->save();
                $msg = ErrorsHelper::getModelError($_authItem);
                if (!empty($msg)) {
                    echo '<pre>';
                    print_r($msg);
                    echo '</pre>';
                }
            }
        } elseif (Yii::$app->request->input('type') == 3) {
            $AuthRoute = new AuthRoute();
            $list = AuthRoute::find()->where(['=', 'item_id', null])->asArray()->all();

            foreach ($list as $key => $value) {
                $_AuthRoute = clone $AuthRoute;
                $_AuthRoute->updateAll([
                    'route_name' => $value['name'],
                ], [
                    'id' => $value['id'],
                ]);
            }
        }
    }
}
