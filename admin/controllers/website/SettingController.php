<?php

/**
 * @Author: Wang Chunsheng 2192138785@qq.com
 * @Date:   2020-03-28 23:43:29
 * @Last Modified by:   Wang chunsheng  email:2192138785@qq.com
 * @Last Modified time: 2022-10-28 16:49:44
 */

namespace admin\controllers\website;

use admin\controllers\AController;
use common\helpers\ImageHelper;
use common\helpers\ResultHelper;
use common\models\WebsiteStationGroup;
use diandi\admin\acmodels\AuthItemChild;
use diandi\admin\acmodels\AuthRoute;
use diandi\admin\components\Configs;
use diandi\admin\models\AuthItem;
use Yii;

/**
 * Class SiteController.
 */
class SettingController extends AController
{
    public $modelClass = '';

    protected array $authOptional = ['info','ceshi'];

    public int $searchLevel = 0;

    function actionCeshi():array
    {
        $config = Configs::instance();
        /* @var $manager \yii\rbac\BaseManager */
        $manager = $config::authManager();
        $list = $manager->getPermissionsByUser(114);
//        print_r($list);
//        修复child 的item_id
//        $listChild =  AuthItemChild::find()->where(['child_type'=>0])->asArray()->all();
//        foreach ($listChild as $item) {
//            $item_id = AuthRoute::find()->where(['name'=> $item['child']])->select('item_id')->scalar();
//            if ($item_id){
//                AuthItemChild::updateAll([
//                    'item_id'=> $item_id
//                ],[
//                    'id'=>$item['id']
//                ]);
//            }else{
//                AuthItemChild::deleteAll(['id'=>$item['id']]);
//            }
//        }

//                修复路由的item_id
//        $listRoutes = AuthRoute::find()->asArray()->all();
//
//        $AuthItem = new \diandi\admin\acmodels\AuthItem();
//        foreach ($listRoutes as $listRoute) {
//            $item_id = $AuthItem::find()->where(['name'=> $listRoute['name']])->select('id')->scalar();
//            AuthRoute::updateAll([
//                'item_id'=> $item_id
//            ],[
//                'id'=>$listRoute['id']
//            ]);
//        }
        return ResultHelper::json(200,'ok',$list);
    }

    /**
     * {@inheritdoc}
     */
    public function actions(): array
    {
        return [
            'website' => [
                'class' => \yii2mod\settings\actions\SettingsAction::class,
                'view' => 'website',
                'successMessage' => '保存成功',
                // also you can use events as follows:
                'on beforeSave' => function ($event) {
                    // your custom code
                },
                'on afterSave' => function ($event) {
                    // your custom code
                },
                'modelClass' => \common\models\forms\Website::class,
            ],
        ];
    }

    public function actionConfig(): array
    {
        $settings = Yii::$app->settings;
        foreach (Yii::$app->request->input('Website') as $key => $value) {
            $settings->set('Website', $key, $value);
        }

        $info = $settings->getAllBySection('Website');

        return ResultHelper::json(200, '设置成功', $info);
    }

    public function actionInfo(): array
    {
        $settings = Yii::$app->settings;
        $settings->invalidateCache();
        $info = $settings->getAllBySection('Website');
        $isImg = Yii::$app->request->input('isImg',0);
        //过滤敏感信息
        $url = Yii::$app->request->input('url','www.dandicloud.cn');
        $url = str_replace('http','',$url);
        $url = str_replace('https','',$url);
        [$url,] = explode('/',$url);
        [$url,] = explode(':',$url);
        $infoUrl = [];
        if (!in_array($url,['www.ddiot.com','www.dandicloud.cn','www.ddicms.cn'])){
            $infoUrl = WebsiteStationGroup::find()->where(['domain_url'=>$url])->asArray()->one();
            $infoUrl['is_send_code'] = $info['is_send_code'];
        }
        $list = (array)$infoUrl?:(array)$info;

        if ((int) $isImg === 1){
            unset(
                $list['qcloud_sdk_app_id'],
                $list['qcloud_secret_id'],
                $list['qcloud_secret_key'],
                $list['qcloud_sign_name'],
                $list['sign_name'],
                $list['site_status'],
                $list['template_code'],
                $list['access_key_id'],
                $list['access_key_secret'],
                $list['aliyun_access_key_id'],
                $list['aliyun_access_key_secret'],
                $list['aliyun_sign_name'],
                $list['aliyun_template_code']);

            //没有登录的登录页使用
            if (isset($list['blogo'])){
                $list['blogo'] = ImageHelper::tomedia($list['blogo']);
            }

            if (isset($list['loginbg'])){
                $list['loginbg'] = ImageHelper::tomedia($list['loginbg']);
            }

            if (isset($list['flogo'])){
                $list['flogo'] = ImageHelper::tomedia($list['flogo']);
            }
        }
        return ResultHelper::json(200, '获取成功', $list);
    }
}
