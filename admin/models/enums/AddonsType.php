<?php

/**
 * @Author: Wang Chunsheng 2192138785@qq.com
 * @Date:   2020-03-13 01:01:58
 * @Last Modified by:   Wang chunsheng  email:2192138785@qq.com
 * @Last Modified time: 2021-06-04 08:58:19
 */

namespace admin\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 * 插件类型
 */
class AddonsType extends BaseEnum
{
    const          ADDONS     = 1;
    const          PLUGINS  = 2;

    const          SYS    = 3;
    
    /**
     * @var string message category
     * You can set your own message category for translate the values in the $list property
     * Values in the $list property will be automatically translated in the function `listData()`
     */
    public static $messageCategory = 'App';

    /**
     * @var array
     */
    public static $list = [
        self::ADDONS=>"基础插件",
        self::PLUGINS=>"系统插件",
        self::SYS=>"系统服务"
    ];
    
}       