<?php

/**
 * @Author: Wang chunsheng  email:2192138785@qq.com
 * @Date:   2024-02-09 00:46:02
 * @Last Modified by:   Wang chunsheng  email:2192138785@qq.com
 * @Last Modified time: 2024-02-09 01:03:39
 */


namespace common\behaviors;

use common\services\common\WebsiteGroup;
use Yii;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\web\UnauthorizedHttpException;

/**
 * 接口数据返回行为
 */
class BeforeSend extends Behavior
{
    /**
     * {@inheritdoc}
     */
    public function events()
    {
        return [
            'beforeSend' => 'beforeSend',
        ];
    }

    /**
     * 格式化返回
     *
     * @param $event
     * @throws InvalidConfigException
     */
    public function beforeSend($event)
    {
        if (YII_DEBUG && isset(Yii::$app->controller->module->id) && Yii::$app->controller->module->id === "debug") {
            return;
        }

        $response = $event->sender;

        $origin = Yii::$app->request->headers['origin'];

        /**
         * 放行授权域名
         */
        $isAllowed  = WebsiteGroup::checkHeaderOrigin($origin);
        if ($isAllowed){
            $response->headers->set('Access-Control-Allow-Origin', $origin);
        }



        // 加入ip黑名单
//        Yii::$app->request->userIP

        $response->format = yii\web\Response::FORMAT_JSON;
        $response->statusCode = 200;
    }
}
