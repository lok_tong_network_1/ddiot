<?php
/**
 * @Author: Wang chunsheng  email:2192138785@qq.com
 * @Date:   2022-07-11 17:40:59
 * @Last Modified by:   Wang chunsheng  email:2192138785@qq.com
 * @Last Modified time: 2022-07-11 18:38:05
 */
return [
    'app_id'   => env('WECHATOPENPLATFORMCONFIG_APP_ID'),
    'secret'   => env('WECHATOPENPLATFORMCONFIG_SECRET'),
    'token'    => env('WECHATOPENPLATFORMCONFIG_TOKEN'),
    'aes_key'  => env('WECHATOPENPLATFORMCONFIG_AES_KEY')
];
