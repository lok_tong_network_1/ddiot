<?php

namespace common\models\forms;

use yii\base\Model;

class App extends Model
{
    public $is_showall = false;
    public $id;

    public $bloc_id;
    /**
     * @var string application name
     */
    public $android_ver;
    public $android_url;
    public $ios_ver;
    public $ios_url;
    public $partner;
    public $partner_key;
    public $paysignkey;
    public $app_id;
    public $app_secret;


    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [[
                'android_ver',
                'android_url',
                'ios_ver',
                'ios_url',
                'partner',
                'partner_key',
                'paysignkey',
                'app_id',
                'app_secret'
            ], 'string'],
            [['id', 'bloc_id'], 'integer'],

        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'android_ver'=>'安卓版本',
            'android_url'=>'安卓最新版地址',
            'ios_ver'=>'ios版本',
            'ios_url'=>'ios最新版地址',
            'partner'=>'财付通商户号',
            'partner_key'=>'财付通密钥',
            'paysignkey'=>'支付签名密钥',
            'app_id'=>'微信开放平台app_id',
            'app_secret'=>'微信开放平台app_secret'
        ];
    }
}