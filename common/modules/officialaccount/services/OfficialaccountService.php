<?php

namespace common\modules\officialaccount\services;

use common\helpers\ErrorsHelper;
use common\helpers\loggingHelper;
use common\helpers\ResultHelper;
use common\models\DdUser;
use common\modules\officialaccount\models\OfficialaccountQrcode;
use common\modules\officialaccount\models\OfficialaccountWechatConf;
use common\modules\openWeixin\models\enums\OpenWeixinAuthCode;
use common\services\BaseService;
use diandi\addons\models\form\Wechat;
use diandi\addons\services\addonsService;
use Yii;
use yii\base\ErrorException;
use yii\base\InvalidConfigException;
use yii\db\Exception;
use yii\helpers\Json;

class OfficialaccountService extends BaseService
{
    /**
     * 获取操作实例，0 公众号 1第三方平台
     * @param int $type
     * @return mixed
     * @throws InvalidConfigException
     */
    static function getWechatApp(int $type = 0): mixed
    {
        if ($type === 0) {
            $wechat = Yii::$app->wechat->app;
        } else {
            $wechat = Yii::$app->getModule('openWeixin')->get('OpenApp')->getApp();
        }
        return $wechat;
    }

    static function getBlocId(): int
    {
        $user_id = Yii::$app->user->identity->user_id ?? 0;
        return DdUser::find()->where(['id' => $user_id])->select('bloc_id')->scalar();
    }

    /**
     * 获取当前公众号配置信息
     * @return array
     */
    static function getWechatConf(): array
    {
        $bloc_id = self::getBlocId();
        $model = new Wechat();

        $conf = OfficialaccountWechatConf::find()->where(['bloc_id' => $bloc_id])->with(['open'])->asArray()->one();

        $conf['app_id'] = $model->decodeConf($conf['app_id']);
        $conf['token'] = $model->decodeConf($conf['token']);
        $conf['aes_key'] = $model->decodeConf($conf['aes_key']);
        $conf['secret'] = $model->decodeConf($conf['secret']);
        $conf['headimg'] = $model->decodeConf($conf['headimg']);

        if ($conf['open']) {
            $conf['open']['authorizer_appid'] = addonsService::hideStr($conf['open']['authorizer_appid'], 5, 10);
            $conf['open']['authorizer_access_token'] = addonsService::hideStr($conf['open']['authorizer_access_token'], 10, 40);
            $conf['open']['authorizer_refresh_token'] = addonsService::hideStr($conf['open']['authorizer_refresh_token'], 10, 40);
            $func_info = Json::decode($conf['open']['func_info']);

            $func_infos = [];
            foreach ($func_info as $item) {
                $func_infos[] =OpenWeixinAuthCode::getLabel($item['funcscope_category']['id']);
            }
            $conf['open']['func_info'] = $func_infos;
        }



        return $conf;
    }

    /**
     * 创建场景二维码
     * @throws InvalidConfigException
     */
    static function createQrcode($timeType,$scene): array
    {
        // Array
        // (
        //     [ticket] => gQFD8TwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAyTmFjVTRWU3ViUE8xR1N4ajFwMWsAAgS2uItZAwQA6QcA
        //     [expire_seconds] => 518400
        //     [url] => http://weixin.qq.com/q/02NacU4VSubPO1GSxj1p1k
        // )
        try {
            $expire_seconds = 6 * 24 * 3600;
            if ($timeType === 0){
                $Res = self::getWechatApp(1)->qrcode->temporary($scene, $expire_seconds);
            }else{
                $expire_seconds = 0;
                $Res = self::getWechatApp(1)->qrcode->forever($scene);
            }

            $OfficialaccountQrcode = new OfficialaccountQrcode();
            $OfficialaccountQrcode->load([
                'type' =>$timeType,
                'extra' =>0,
                'qrcid' =>0,
                'scene_str' =>$scene,
                'name' =>'officialaccount',
                'keyword' =>'',
                'model' =>'',
                'ticket' =>$Res['ticket'],
                'url' =>$Res['url'],
                'expire' =>$expire_seconds,
                'subnum' =>0,
                'status' =>1,
            ],'');
            if (!$OfficialaccountQrcode->save()){
                $msg = ErrorsHelper::getModelError($OfficialaccountQrcode);
                throw new ErrorException($msg);
            }
            $url = self::getWechatApp(1)->qrcode->url($Res['ticket']);
            return ResultHelper::json(200, '生成成功', [
                'url' => $url
            ]);
        }catch (Exception $e){
            return ResultHelper::json(400, $e->getMessage(), (array)$e);
        } catch (\Throwable $e) {
            return ResultHelper::json(400, $e->getMessage(), (array)$e);
        }

    }
}