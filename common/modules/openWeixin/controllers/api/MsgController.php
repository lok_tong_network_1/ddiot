<?php
/**
 * @Author: Wang chunsheng  email:2192138785@qq.com
 * @Date:   2020-11-14 22:17:14
 * @Last Modified by:   Wang chunsheng  email:2192138785@qq.com
 * @Last Modified time: 2022-07-11 18:52:57
 */

namespace common\modules\openWeixin\controllers\api;

use api\controllers\AController;
use common\helpers\loggingHelper;
use common\modules\officialaccount\services\FansService;
use common\modules\officialaccount\services\MessageService;
use common\modules\openWeixin\services\OpenWechatAuthService;
use common\modules\openWeixin\services\OpenWechatMsgService;
use EasyWeChat\Factory;
use EasyWeChat\Kernel\Exceptions\BadRequestException;
use EasyWeChat\Kernel\Exceptions\InvalidArgumentException;
use EasyWeChat\Kernel\Exceptions\InvalidConfigException;
use EasyWeChat\Kernel\Messages\Text;
use EasyWeChat\OpenPlatform\Server\Guard;
use Yii;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\web\NotFoundHttpException;

class MsgController extends AController
{
    protected array $authOptional = ['event', 'open'];

    public $modelClass = 'api\modules\officialaccount\models\DdWechatFans';

    public $defaultAction = 'event';

    public function actionOpen(): void
    {
        $request = Yii::$app->request;
        $app = Yii::$app->wechat->getApp();
        loggingHelper::writeLog('openWeixin', 'actionOpen', '事件监听处理', [
            'getMethod' => $request->getMethod(),
        ]);

        $configPath = Yii::getAlias('@common/config/wechat.php');
        $config = [];
        if (file_exists($configPath)) {
            $config = require_once $configPath;
        }
        $data = [
            'app_id' => $config['app_id'],
            'secret' => $config['secret'],
            'token' => $config['token'],
            'aes_key' => $config['aes_key'],
        ];
        loggingHelper::writeLog('openWeixin', 'actionOpen', '配置信息', [
            'data' => $data,
            'config' => $config
        ]);
        $openPlatform = Factory::openPlatform($data);
        $server = $openPlatform->server;

        loggingHelper::writeLog('openWeixin', 'actionOpen', '服务数据', [
            'server' => $server->serve(),
            'data' => $server
        ]);
        $response = $app->server->serve();
        $response->send();
    }

    /**
     * 微信请求关闭CSRF验证
     *
     */
    // public $enableCsrfValidation = false;

    /**
     * 只做微信公众号激活，不做其他消息处理.
     * https://dev.hopesfire.com/api/officialaccount/msg/event?store_id=81&bloc_id=32.
     * @param $appid
     * @return Text|string|void
     * @throws Exception
     * @throws BadRequestException
     * @throws InvalidArgumentException
     * @throws InvalidConfigException
     * @throws \ReflectionException
     * @throws \Throwable
     * @throws ErrorException
     */
    public function actionEvent($appid)
    {
        $request = Yii::$app->request;
        $openPlatform = Yii::$app->wechat->openPlatform;
        loggingHelper::writeLog('openWeixin', 'actionEvent', '事件监听处理', [
            'appid' => $appid,
            'data' => Yii::$app->request->input(),
            'getMethod' => $request->getMethod(),
            'body' => Yii::$app->request->getRawBody()
        ]);

        $server = $openPlatform->server;
        $from_xml = Yii::$app->request->getRawBody();
        $nonce = Yii::$app->request->input('nonce');
        $timeStamp = Yii::$app->request->input('timestamp');
        $msg_sign = Yii::$app->request->input('msg_signature');
        $message = $openPlatform->server->getMessage();
        loggingHelper::writeLog('openWeixin', 'message', '解密内容', [
            'message' => $message
        ]);
        if (key_exists('MsgType',$message)){
            switch ($message['MsgType']) {
                case 'text':
                    // 文本消息处理
                    if ($message['Content'] == 'TESTCOMPONENT_MSG_TYPE_TEXT') {
                        return 'TESTCOMPONENT_MSG_TYPE_TEXT_callback';
                    }

                    // 若为事件地理位置选择，则回复相应消息
                    if (str_contains($message['Content'], 'LOCATION_INFO')) {
                        return 'TESTCOMPONENT_MSG_TYPE_LOCATION_callback';
                    }

                    // 若为事件普通地理位置，回复相应消息
                    if (str_contains($message['Content'], 'LOCATION')) {
                        return 'TESTCOMPONENT_MSG_TYPE_LOCATION_callback';
                    }
                    break;
                case 'event':
                    // 事件消息处理
                    if ($message['Event'] == 'VIEW') {
                        return new Text($message['EventKey']);
                    }elseif ($message['Event'] == 'SCAN' && $message['EventKey'] === 'login'){
                        OpenWechatAuthService::autoScanLogin($message['FromUserName']);
                    }elseif ($message['Event'] == 'SCAN' && $message['EventKey'] === 'userbind'){
                        OpenWechatAuthService::autoUserBind($message['FromUserName'],$message['Ticket']);
                    }
                    break;
            }
        }else{
            // 处理授权成功事件
            $server->push(function ($message) use ($from_xml, $nonce, $timeStamp, $msg_sign) {
                loggingHelper::writeLog('openWeixin', Guard::EVENT_AUTHORIZED, '事件消息处理', [
                    'message' => $message
                ]);


                $Res = OpenWechatMsgService::decodeMsg($from_xml, $nonce, $timeStamp, $msg_sign);
                loggingHelper::writeLog('openWeixin', Guard::EVENT_AUTHORIZED, 'xml解析', [
                    'Res' => $Res
                ]);
                return null;

            }, Guard::EVENT_AUTHORIZED);

            // 处理授权更新事件
            $server->push(function ($message) use ($from_xml, $nonce, $timeStamp, $msg_sign) {
                loggingHelper::writeLog('openWeixin', Guard::EVENT_UPDATE_AUTHORIZED, '事件消息处理', [
                    'message' => $message
                ]);
                $Res = OpenWechatMsgService::decodeMsg($from_xml, $nonce, $timeStamp, $msg_sign);

                loggingHelper::writeLog('openWeixin', Guard::EVENT_AUTHORIZED, 'xml解析', [
                    'Res' => $Res
                ]);
                return null;
            }, Guard::EVENT_UPDATE_AUTHORIZED);

            // 处理授权取消事件
            $server->push(function ($message) use ($from_xml, $nonce, $timeStamp, $msg_sign) {
                loggingHelper::writeLog('openWeixin', Guard::EVENT_UNAUTHORIZED, '事件消息处理', [
                    'message' => $message
                ]);
                $Res = OpenWechatMsgService::decodeMsg($from_xml, $nonce, $timeStamp, $msg_sign);
                loggingHelper::writeLog('openWeixin', Guard::EVENT_AUTHORIZED, 'xml解析', [
                    'Res' => $Res
                ]);
                return null;
            }, Guard::EVENT_UNAUTHORIZED);

            //快速注册完成
            $server->push(function ($message) use ($from_xml, $nonce, $timeStamp, $msg_sign) {
                loggingHelper::writeLog('openWeixin', Guard::EVENT_THIRD_FAST_REGISTERED, '事件消息处理', [
                    'message' => $message
                ]);


                $Res = OpenWechatMsgService::decodeMsg($from_xml, $nonce, $timeStamp, $msg_sign);
                loggingHelper::writeLog('openWeixin', Guard::EVENT_THIRD_FAST_REGISTERED, 'xml解析', [
                    'Res' => $Res
                ]);
                return null;

            }, Guard::EVENT_THIRD_FAST_REGISTERED);
        }


        // 将响应输出
        $server->serve()->send();
        exit();
    }

    /**
     * 事件处理.
     *
     * @param $message
     *
     * @return bool|mixed
     *
     * @throws NotFoundHttpException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     */
    protected function event($message): mixed
    {
        Yii::$app->params['msgHistory']['event'] = $message['Event'];
        $FansService = new FansService();
        $MessageService = new MessageService();
        loggingHelper::writeLog('openWeixin', 'subscribe', '事件开始', [
            'message' => $message,
        ]);
        switch ($message['Event']) {
            // 关注事件
            case 'subscribe':
                loggingHelper::writeLog('openWeixin', 'subscribe', '关注事件', [
                    'msg' => $message,
                ]);
                $FansService->follow($message['FromUserName']);

                // 判断是否是二维码关注
                // if ($qrResult = Yii::$App->wechatService->qrcodeStat->scan($message)) {
                //     $message['Content'] = $qrResult;
                //     $MessageService->setMessage($message);

                //     return $MessageService->text();
                // }

                // return $MessageService->follow();
                break;
            // 取消关注事件
            case 'unsubscribe':
                loggingHelper::writeLog('openWeixin', 'subscribe', '取消关注事件', [
                    'FromUserName' => $message['FromUserName'],
                ]);
                $FansService->unFollow($message['FromUserName']);

                return false;
                break;
            // 二维码扫描事件
            case 'SCAN':
                // if ($qrResult = Yii::$App->wechatService->qrcodeStat->scan($message)) {
                //     $message['Content'] = $qrResult;
                //     $MessageService->setMessage($message);

                //     return $MessageService->text();
                // }
                break;
            // 上报地理位置事件
            case 'LOCATION':

                //TODO 暂时不处理

                break;
            // 自定义菜单(点击)事件
            case 'CLICK':
                $message['Content'] = $message['EventKey'];
                $MessageService->setMessage($message);

                return $MessageService->text();
                break;
        }

        return false;
    }
}
