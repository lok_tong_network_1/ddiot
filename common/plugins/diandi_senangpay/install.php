<?php

namespace common\plugins\diandi_senangpay;

use common\components\addons\AddonsInstall;

/**
 * 安装.
 *
 * Class Install
 */
class Install extends AddonsInstall
{
    public $addons = 'diandi_senangpay';
}
