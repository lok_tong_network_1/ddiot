<?php

namespace common\plugins\diandi_senangpay;

use common\components\addons\AddonsUninstall;


/**
 * 卸载.
 *
 * Class Install
 */
class uninstall extends AddonsUninstall
{
    public string $addons = 'diandi_senangpay';
}
