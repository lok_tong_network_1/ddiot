<?php

namespace common\services\common;

use common\models\WebsiteStationGroup;
use common\services\BaseService;
use Yii;

class WebsiteGroup extends BaseService
{
    /**
     * ��������
     * @return array
     */
    static function getAllowedDomain(): array
    {
        $urls = WebsiteStationGroup::find()->select('domain_url')->column();
        foreach ($urls as &$url) {
            if (!(str_starts_with($url, 'Http://')) && !(str_starts_with($url, 'https://'))) {
                $url = 'https://' . $url;
            }
        }
        return $urls ?? [];
    }

    /**
     * ��������
     * @return string
     */
    static function getAllowedDomainStr(): string
    {

        $urls = WebsiteStationGroup::find()->select('domain_url')->column();
        foreach ($urls as &$url) {
            if (!(str_starts_with($url, 'Http://')) && !(str_starts_with($url, 'https://'))) {
                $url = 'https://' . $url;
            }
        }

        return implode(',', $urls ?? []);
    }

    /**
     * @param $origin
     * @return bool
     */
    static function checkHeaderOrigin($origin): bool
    {

        if (in_array($origin,self::getAllowedDomain())){
            return true;
        }else{
            return false;
        }
    }

}