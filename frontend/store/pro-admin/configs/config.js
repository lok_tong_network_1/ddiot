/**
 * @Author: Wang chunsheng  email:2192138785@qq.com
 * @Date:   2021-11-29 23:22:59
 * @Last Modified by:   Wang chunsheng  email:2192138785@qq.com
 * @Last Modified time: 2024-01-28 22:23:44
 */

$siteName = '智慧小店',
$siteDesc = '智慧小店-茶室，棋牌，台球桌等无人场所',
$apiUrl = 'https://www.dandicloud.cn',
$siteUrl = 'https://www.dandicloud.cn'
